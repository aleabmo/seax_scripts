#!/bin/bash

##########################
#
# Autors:
#	Aleix Abengochea
#	Omara Aibar
#
##########################

##### TODO
# Check user root
# Check iw i jp 
# check interfaces for wifi

#Constants del script
version=0.1.1
author1="Aleix Abengochea"
author2="Omara Aibar"
programname=$0

#Funcio pet tal de imprimir el usage
function usage {
  echo "usage: $programname [-vh] [-d database]"
  echo "  -v      display version"
  echo "  -h      display help"
  echo "  -d database   specify input file database"
  echo "  -i interficie   specify interficie wifi"
  exit 1
}

#Funcio per tal d'imprimir l'ajuda
function show_help {
  echo "
  Aquest programa serveix per comparar un scan que es realitza al moment, amb una base de dades introduiida per parametre
  Es genera un fitxer default_scan.json, el qual s'utilitza per comparar amb la base de dades
  
  1. Executa un scan sobre la interficie -i [interface]
  2. Compara aquest scan amb la db especificada amb la comanda -d [database]
  3. Mostra el punt mes proper.
  
  Com a minim sha de executar amb -d [database] -i [interface].
  *Els gaps s'omplen amb -100 com mostrava al example

  Es necessiten permisos de root per tal de poder fer el scan de iw, i tenir els paquets jq i iw
  "
  usage
}

#Funcio per tal d'imprimir la versio
function show_version {
  echo " Version: $version"
  echo " Authors: Made by $author1 & $author2"
  exit
}

#Funcio per tal de comprovar si hi ha paquets instalats
function check_packages {
  aux=$(dpkg-query -W -f='${db:Status-Status}\n' $1)
  if [ "$aux" == "installed" ]
  then
    echo " Paquet($1) Status: $(dpkg-query -W -f='${db:Status-Status}\n' $1)"
  else
    echo " Paquet($1) Status: $(dpkg-query -W -f='${db:Status-Status}\n' $1)"
    exit 1
  fi
}

#Funcio per tal de guardar un punt a la base de dades
function save_point {

        #Generem el fitxer auxiliar on guardarem el scan actual
        file="default_scan.json"
        echo "{ \"db\":{}}" > $file

        #Generem el scan i guardem a les variables
        output=$(iw dev $inter scan)
        #Fem el parsing com al script scan.sh
        scan=($(echo $output | grep -Po "(([0-9a-fA-F]{2}[:]){5}([0-9a-fA-F]){2})"))
        sscan=($(echo $output | grep -Po "signal: -([0-9])*" | awk '{print$2}'))

        #Proves amb fitxer
        #output="wea.txt"
        #scan=($(cat $output | grep -Po "(([0-9a-fA-F]{2}[:]){5}([0-9a-fA-F]){2})"))
        #sscan=($(cat $output | grep -Po "signal: -([0-9])*" | awk '{print$2}'))

        aux=$(jq .db.$1 $file)
        if [ "$aux" == "null" ]
        then
                echo "$(jq '.db += {"'$1'":{}}' $file )" > $file
        fi

        date=$(date +%d-%m-%Y_%H-%M-%S)
        echo "$(jq '.db .'$1' += {"'$date'":[]}' $file )" > $file

        for ((i=0;i<${#scan[@]};i++))
        do
                echo "$(jq '.db .'$1' ."'$date'" += [{"bssid":"'${scan[$i]}'", "rssi":'${sscan[$i]}'}]' $file )" > $file
        done

}


###########
#
# Workflow
#
###########

#If else per tal de controlar el flux del programa.
if [ "$#" -lt 1 ]
then
	usage
fi

while [ "$1" != "" ]; do
  case $1 in
    -h )
      shift
      show_help
      ;;

    -v )
      shift
      show_version
      ;;

    -d )
      shift
      db=$1
      ;;

    -i )
      shift
      inter=$1
      ;;

    * )
      echo " Parametre de opció incorrecte, comprovar el help per més informació (-h)" 
      exit
  esac
  shift
done

###########
#
# Execucio
#
###########

#Comprovem que els paquets necessaris estiguin instalats al sistema
check_packages "jq"
check_packages "iw"

#Necessitem permisos root per tal de fer el iw
echo " --------------------------------------------------------------------------------"
echo " Comprovant que tinguem permissos de root"
echo " --------------------------------------------------------------------------------"
#Tambe es pot fer amb id -u pero es molt mes lent.
if [[ $EUID -ne 0 ]]; then
  echo " Aquest script requereix ser executat com a root"
  exit 1
fi
echo " Permissos -- OK"
echo " "

#Comprovem que hi hagi interficies
echo " --------------------------------------------------------------------------------"
echo " Comprovant que hi hagi interficies i estiguin actives"
echo " --------------------------------------------------------------------------------"

aux=$(ip link show | grep $inter | wc -l)
if [ $aux -gt 0 ]
then
  echo " Hi ha interficie activa"	
else
  echo " No hi ha interficie activa"
  exit 1
fi

#Obtenim el scan actual
save_point "actual"
scan="default_scan.json"

#Obtenim el id del scan actual
scan_id=$(jq '.db | keys[]' $scan)
scan_data=$(jq '.db .'$scan_id' | keys[]'  $scan)

#Setegem com distancia a vencer.
actual=10000000
acutal_id=""

#Per cada id nova a la db comparem tots els bssid del scan
for db_id in $(jq '.db | keys[]' $db)
do
        #Per cada bssid que tinguem al scan
        aux=0
        for lol in $(jq '.db .'$db_id' | keys[]' $db)
        do

                for bssid in $(jq '.db .'$scan_id' .'$scan_data' | .[] | .bssid' $scan)
                do
                        #rssi del db
                        db_rssi=$(jq '.db .'$db_id' .'$lol' | .[] | select(.bssid=='$bssid') | .rssi' $db)
                        #Si no trobem el arxiu fem un filling dels gaps amb -100.
                        if [ "$db_rssi" == "" ]
                        then
                                db_rssi=-100
                        fi
                        #rssi del scan
                        scan_rssi=$(jq '.db .'$scan_id' .'$scan_data' | .[] | select(.bssid=='$bssid') | .rssi' $scan)

                        ###Calculem la distancia

                        #Calculem la resta
                        resta=$((db_rssi-scan_rssi))
                        #Calculem la potencia amb awk
                        pot=$(awk "BEGIN{print $resta * $resta}")
                        #Acumulem el resultat
                        aux=$((aux+pot))

                        echo "  db_rssi:  $db_rssi, scan_rssi: $scan_rssi, resta: $resta,pot: $pot, aux: $aux"
                done

                #Calculem la arrel del resultat
                result=$(awk "BEGIN{print sqrt($aux)}")
                echo "Result: $result"

                #Comprovem si el resultat que acabem de calcular es el mes petit fins ara.
                if (awk 'BEGIN {exit !('$actual' >= '$result')}')
                then
                        #Si es aixi, guardem el resultat com l'actual a vencer
                        actual=$result
                        #Guardem tambe la etiqueta del punt
                        acutal_id=$db_id
                fi
        done
done
#Mostrem el resultat per pantalla
echo "El punt mes proper es $acutal_id amb una distancia: $actual"
