#!/bin/bash

##########################
#
# Autors:
#	Aleix Abengochea
#	Omara Aibar
#
##########################

##### TODO
# Check user root
# Check iw i jp 
# check interfaces for wifi

#Constants del script
file="default_db.json"
version=0.1.1
author1="Aleix Abengochea"
author2="Omara Aibar"
programname=$0

#Funcio pet tal de imprimir el usage
function usage {
  echo "usage: $programname [-vh] [-p nom] [-d database] [-i interface]"
  echo "  -v      display version"
  echo "  -h      display help"
  echo "  -p nom   specify name of point"
  echo "  -d database   specify input file database"
  echo "  -i interficie   specify wifi interface"
  exit 1
}

#Funcio per tal d'imprimir l'ajuda
function show_help {
  echo "
  HELP

  Aquest script serveix per generar una DB sobre lectures wifi

  1. El script realitza un scan de les xarxes wifi sobre la interficie -i [interficie].
  2. Guarda un punt amb el nom del parametre -p [nom] a una base de dades introduida per paramtre -d [database].
      *Si no s'introdueix cap base de dades, contempla una base de ddades per defecte de nom default_db.json
  
  Per tal de generar una base de dades amb diversos punts, s'ha d'executar el script diversos cops amb la db que es vulgui.
  Hem pensat que es un sistema més flexible.

  !Per tal de fer el scan es necesiten permisos de root i una interificie wifi.

  "
  usage
}

#Funcio per tal d'imprimir la versio
function show_version {
  echo " Version: $version"
  echo " Authors: Made by $author1 & $author2"
  exit
}

#Funcio per tal de comprovar si hi ha paquets instalats
function check_packages {
    aux=$(dpkg-query -W -f='${db:Status-Status}\n' $1)
    if [ "$aux" == "installed" ]
    then
        echo " Paquet($1) Status: $(dpkg-query -W -f='${db:Status-Status}\n' $1)"
    else
	exit 1
    fi
}

#Funcio per tal de guardar un punt a la base de dades
function save_point {

        #Obtenim el scan del wifi
        output=$(iw dev $inter scan)
        #Fem un grep amb una regex per al bssid i el signal
        #La regex no es exacta per una mac pero aquesta simple serveix
        scan=($(echo $output | grep -Po "(([0-9a-fA-F]{2}[:]){5}([0-9a-fA-F]){2})"))
        sscan=($(echo $output | grep -Po "signal: -([0-9])*" | awk '{print$2}'))

        #Proves amb fitxer
        #output="wea.txt"
        #scan=($(cat $output | grep -Po "(([0-9a-fA-F]{2}[:]){5}([0-9a-fA-F]){2})"))
        #sscan=($(cat $output | grep -Po "signal: -([0-9])*" | awk '{print$2}'))

        #Si no existeix el punt ja al sistema, el creem. Inicialitzan el db buit per defecte.
        aux=$(jq .db.$1 $file)
        if [ "$aux" == "null" ]
        then
                echo "$(jq '.db += {"'$1'":{}}' $file )" > $file
        fi

        #Agafem el date com a id diferenciador per multiples lectures en un mateix punt.
        date=$(date +%d-%m-%Y_%H-%M-%S)
        echo "$(jq '.db .'$1' += {"'$date'":[]}' $file )" > $file

        #Per cada lectura que hem fet, guardem el bssid i el rssi
        for ((i=0;i<${#scan[@]};i++))
        do
                echo "$(jq '.db .'$1' ."'$date'" += [{"bssid":"'${scan[$i]}'", "rssi":'${sscan[$i]}'}]' $file )" > $file
        done

}

########
# 
# Workflow
#
########

#If else per tal de controlar el flux del programa.
if [ "$#" -lt 1 ]
then
	usage
fi

#Tipic while que filtra els diferents args.
#No es perfecte, te algunes errades de comprovacio
while [ "$1" != "" ]; do
case $1 in
  -h )
    shift
    show_help
    ;;

  -v )
    shift
    show_version
    ;;

	-p )	
    shift
    id=$1
		    ;;

	-d )	
    shift
    file=$1
		;;

	-i )	
    shift
    inter=$1
		;;

  * )         
    echo " Parametre de opció incorrecte, comprovar el help per més informació (-h)" 
		exit
  esac
  shift
done

########
# 
# Execucio
#
########

#Comprovem que els paquets necessaris estiguin instalats al sistema
check_packages "jq"
check_packages "iw"

#Necessitem permisos root per tal de fer el iw
echo " --------------------------------------------------------------------------------"
echo " Comprovant que tinguem permissos de root"
echo " --------------------------------------------------------------------------------"
#Tambe es pot fer amb id -u pero es molt mes lent.
if [[ $EUID -ne 0 ]]; then
  echo " Aquest script requereix ser executat com a root"
  exit 1
fi
echo " Permissos -- OK"
echo " "


#Comprovem que hi hagi interficies
echo " --------------------------------------------------------------------------------"
echo " Comprovant que hi hagi interficies i estiguin actives"
echo " --------------------------------------------------------------------------------"

aux=$(ip link show | grep $inter | wc -l)
if [ $aux -gt 0 ]
then
  echo " Hi ha interficie"	
else
  echo " No hi ha interficie connectades"
  exit 1
fi

#Comprovar que existeix el fitxer de base de dades, sino el creem
if [ -f "$file" ]; then
        echo "Afegint data al arxiu: $file."
else
        echo "Creant i afegint data al arxiu: $file."
        echo "{ \"db\":{}}" > $file
fi

#Guardem un punt, no caldria funcio, pero al principi ho teniem diferent i aixi serveix per al segon script.
save_point $id

