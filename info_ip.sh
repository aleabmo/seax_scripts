#!/bin/bash

##########################
#
# Autors:
#	Aleix Abengochea
#	Omara Aibar
#
##########################

#Constants del script
version=0.1.1
author1="Aleix Abengochea"
author2="Omara Aibar"
so=$(lsb_release -ds)
data_inici=$(date +%F)
hora_inici=$(date +%T)

#Fitxer de log 
log="log_ip"      

usage ()
{
  echo ' info_ip.sh -h <help> -v <version>'    #usage del script
  exit
}

#Funció de l'opció -h 
show_help()
{
#text que es mostrara al help
    help="
    Usage:
      ./info_ip.sh [-i] [ip]
      ./info_ip.sh [-hv]
      ./info_ip.sh [--ip-example] [1-3]

    Opciones:
      -h muestra la ayuda de este script
      -v muestra la versión del script
      -i introducir una ip como arguemento
      --ip-example obtiene una ip de ejemplo introduciendo como argumento un numero del 1 al 3:
           1: es la ip de la UPC
           2: es la ip de PcComponentes
           3: es la ip de Google

     README:
        - Para que el script funcione correctamente hay que tener instalados los siguientes paquetes:
              curl (apt-get install curl)
              whois (apt-get install whois)
              nmap (apt-get install nmap)
              dnsutils (apt-get install dnsutils)
	- Hay que ejecutarlo con permisos de administrador
	- Obtiene información sobre la ip indicada

   "

 echo -e "$help"      #Mostra el text del help
  exit
}

#Funció que mostra la versió del script
show_version()
{
  echo " Version: $version"                       #Diu la versió del script
  echo " Authors: Made by $author1 & $author2"    #Diu els autors del script
  exit
}

#Funció que revisa si els paquets necesaris estan instal·lats
check_packages()
{
    aux=$(dpkg-query -W -f='${db:Status-Status}\n' $1)          #Mira l'estat d'un paquet
    if [ "$aux" == "installed" ]				#Si l'estat del  paquet esta installed continuem amb l'execució
    then
        echo " Paquet($1) Status: $(dpkg-query -W -f='${db:Status-Status}\n' $1)"   #Missatge que mostra que el paquet esta instal·lat
    else
        echo " Paquet($1) Status: $(dpkg-query -W -f='${db:Status-Status}\n' $1)"   #Missatge que mostra que el paquet esta instal·lat
        echo " Sortint del programa"
	      exit 1
    fi
}
#Comprovem si el numero de parametres es més petit que 1
if [ "$#" -lt 1 ]     
then
	usage		 #En cas afirmatiu mostrem el usage del script
fi

#Mentres que el primer parametre no estigui buit, comprovem l'opció
while [ "$1" != "" ]; do
case $1 in
        -h )        shift 		#Si es una "h" mostrem el contingut de la funció "show_help"
		    show_help 
                    ;;

        -v )        shift		#Si es una "v" mostrem el contingut de la funció "show_version"
		    show_version
                    ;;

	-i )	    shift		#Si es una "i" hem d'introduir una ip per parametre
		    #Aqui obliguem a ser bash, sino canviar la regex, comprovem que sigui ip valida
		    if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
		      ip=$1
	            else
		      echo "Entra una direcció ip valida"
		      exit 1
		    fi
		    ;;

        #Per tal de que sigui mes facil deuggar o testar.
        --ip-example )      
		    shift
		    case $1 in

			    1)  shift
				ip="147.83.2.135"
			        ;;

			    2)  shift
				ip="104.16.162.71"
			        ;;

			    3)  shift
				ip="8.8.8.8"
			        ;;

			    *)	shift
				ip="147.83.2.135"
				;;
		    esac
                    ;;

	    * )         echo " Parametre de opció incorrecte, comprovar el help per més informació (-h)" 
		    exit
    esac
    shift
done

echo " --------------------------------------------------------------------------------"
echo " Comprovant que els paquets necessaris estiguins instalats"
echo " --------------------------------------------------------------------------------"
check_packages "nmap"
check_packages "curl"
check_packages "whois"
check_packages "dnsutils"
echo " "

#Necessitem permisos root per tal de fer el nmap -sO
echo " --------------------------------------------------------------------------------"
echo " Comprovant que tinguem permissos de root"
echo " --------------------------------------------------------------------------------"
#Tambe es pot fer amb id -u pero es molt mes lent.
if [[ $EUID -ne 0 ]]; then
  echo " Aquest script requereix ser executat com a root"
  exit 1
fi
echo " Permissos -- OK"
echo " "


#Comprovem que hi hagi interficies
echo " --------------------------------------------------------------------------------"
echo " Comprovant que hi hagi interficies i estiguin actives"
echo " --------------------------------------------------------------------------------"

aux=$(ip link show | grep '^[0-9]' | wc -l)
if [ $aux -gt 1 ]
then
  echo " Hi ha interficies"	
  aux=$(ip link show | grep 'UP' | wc -l)
  if [ $aux -gt 1 ]
  then
    echo " Hi ha interfici connectada"
  else
    echo " Interficie no activa"
    exit 1
  fi
else
  echo " No hi ha interficies connectades"
  exit 1
fi


#Comprovem que hi hagi connexio necessari per fer whois i lookup
echo " --------------------------------------------------------------------------------"
echo " Comprovant que hi ha connexió a Internet(8.8.8.8)"
echo " --------------------------------------------------------------------------------"

nc -z 8.8.8.8 53  >/dev/null 2>&1
online=$?
if [ $online -eq 0 ]; then
    echo " Status: Online"
else
    echo " Status: Offline"
fi

#Comprovem que el destí sigui abastable per tal de poder fer nmap
echo " --------------------------------------------------------------------------------"
echo " Comprovant que el destí sigui abastable $ip"
echo " --------------------------------------------------------------------------------"

ping -c 1 -w 2 $ip >/dev/null 2>&1
online=$?
if [ $online -eq 0 ]; then
    echo " Destí abastable"
else
    echo " Destí no abastable"
    exit
fi

 ###############
#	       #
#   Execucio   #
#	       #
###############
echo " --------------------------------------------------------------------------------"
echo " Executant Script"
echo " --------------------------------------------------------------------------------"

dns=$(dig +short -x $ip)
ipdns="$ip ($dns)"
xarxa=$(whois $ip | grep CIDR | tail -n 1 | awk '{print$2}')
xarxa_rang=$(whois $ip | grep NetRange | tail -n 1 | awk -F':' '{print$2}' | awk '{$1=$1;print}')
xarxa_name=$(whois $ip | grep netname | awk '{print$2}')
if [ "$xarxa_name" == "" ] 
then
  xarxa_name=$(whois $ip | grep NetName | awk '{print$2}')
fi
xarxa_print="$xarxa [$xarxa_rang] ($xarxa_name)"
if [ "$xarxa" == "" ] 
then
  xarxa_name="--"
fi

#Extreure informacio de localitzacio

ipinfo=$(curl -s ipinfo.io/$ip)

city=$(echo $ipinfo | grep -Po '"city":.*' | awk -F'"' '{print$4}')
postal=$(echo $ipinfo | grep -Po '"postal":.*' | awk -F'"' '{print$4}')
region=$(echo $ipinfo | grep -Po '"region":.*' | awk -F'"' '{print$4}')
country=$(echo $ipinfo | grep -Po '"country":.*' | awk -F'"' '{print$4}')

loc_print="$city (CP $postal), $region, ($country)"
if [ "$city" == "" ] 
then
  loc_print="--"
fi

coords=$(echo $ipinfo | grep -Po '"loc":.*' | awk -F'"' '{print$4}')
lat=$(echo $coords | awk -F',' '{print$1}')
lon=$(echo $coords | awk -F',' '{print$2}')
timezone=$(echo $ipinfo | grep -Po '"timezone":.*' | awk -F'"' '{print$4}')

coords_print="Latitud $lat i longitud $lon, amb zona horària $timezone"
if [ "$coords" == "" ] 
then
  coords_print="--"
fi

org=$(whois $ip | grep descr | head -n 1 | awk -F':' '{print$2}')
if [ "$org" == "" ] 
then
  org=$(whois $ip | grep OrgName | awk '{print$2}')
fi

#asn=$(whois $ip | grep origin | awk '{print$2}')
asn=$(curl -s ipinfo.io/$ip | grep -Po '"org":.*' | awk -F'"' '{print$4}')
if [ "$asn" == "" ] 
then
  asn=$(whois $ip | grep OriginAS | awk '{print$2}')
fi

#Extreure informacio del sistema operatiu

osdetails=$(nmap -O -T5 --min-rate 1000 --host-timeout 30s $ip | grep "OS details:" | awk -F':' '{print$2}')
if [ "$osdetails" == "" ] 
then
  osdetails="No detectat"
fi

ports_aux=($(nmap $ip -T5 --min-rate 300 -oG - | grep -Po "Ports:.*\t"))
#ports_aux=($(nmap $ip -oG - | grep -Po "Ports:.*\t"))
wea=""

for i in "${ports_aux[@]}" 
do 
	if [ $i != "Ports:" ]
	then
		wea=$(echo $i | awk -F'/' '{print $3"/"$1"    ("$5")" }') 
		ports+="               $wea\n"
	fi
done
#echo -e $ports

data_final=$(date +%F)
hora_final=$(date +%T)

 ###############
#	       #
#    Output    #
#	       #
###############

echo "
 ---------------------------------------------------------------------------------------------------
 Cerca i anàlisi d'una adreça IP realitzada per l'usuari $USER de l'equip $HOSTNAME.
 Sistema operatiu $so.
 Versió del script $version.
 Anàlisi iniciada en data $data_inici a les $hora_inici i finalitzada en data $data_final a les $hora_final.
 ---------------------------------------------------------------------------------------------------
" > $log

echo " 
 --------------------------------------------------------------------------------
 Informació IP
 --------------------------------------------------------------------------------
 Equip:        $ipdns
 Xarxa:        $xarxa_print
 Entitat:$org
 Gestor ASN:   $asn
 Localització: $loc_print
 Coordenades:  $coords_print
 S. Operatiu:  $osdetails
" >> $log
echo -e "Ports: 
$ports" >> $log

#printf " %-15s %-15s\n" "Equip:" $ip >> $log
# printf "%-20s %-20s" "Xarxa:" ($xarxa [$xarxa_rang] ($xarxa_name))
#printf " %-15s %-15s\n" "Entitat:" $org >> $log
#printf " %-15s %-15s\n" "Gestor ASN:" $asn >> $log
#printf " %-15s %s\n" "Localització:" $coords_print >> $log
#printf " %-15s %s" "Coordenades:" $coords_print >> $log
#printf " %-15s %-15s\n" "S. Operatiu:" $osdetails >> $log
#printf " %-15s %-15s\n" "Ports" $ip >> $log
 
